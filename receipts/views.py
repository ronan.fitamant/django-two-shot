from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import NewReceiptForm, NewCategoryForm, NewAccountForm


# Create your views here.
@login_required
def receipt_all(request):
    receipts = Receipt.objects.all().filter(purchaser=request.user)
    context = {
        "all_receipts": receipts,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = NewReceiptForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.purchaser = request.user
            form.save()
        return redirect("home")

    else:
        form = NewReceiptForm()

    context = {"form": form}

    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_lists": list,
    }
    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):
    list = Account.objects.filter(owner=request.user)
    context = {
        "account_lists": list,
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = NewCategoryForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.owner = request.user
            form.save()
        return redirect("category_list")

    else:
        form = NewCategoryForm()

    context = {"form": form}

    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = NewAccountForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.owner = request.user
            form.save()
        return redirect("account_list")

    else:
        form = NewAccountForm()

    context = {"form": form}

    return render(request, "receipts/create_account.html", context)
